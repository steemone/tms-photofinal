
import UIKit
import SwiftyKeychainKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var startButtonOutlet: UIButton!
    @IBOutlet weak var registerButtonOutlet: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    var password: String = ""
    let keychain = Keychain(service: "-.HW17-2---Cloude-photo")
    let accessTokenKey = KeychainKey<String>(key: "accessToken")
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        clearKeyChaing()
        spinner.isHidden = true
        do {
            guard let value = try keychain.get(accessTokenKey) else {return}
            self.password = value
            if self.password == value {
                registerButtonOutlet.isHidden = true
            }
        } catch let error {
            debugPrint(error)
        }
        print(password)
        
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        showAlert(title: "Warning", text: "Enter your password")
    }
    
    @IBAction func registrationButton(_ sender: UIButton) {
        showRegAlert(title: "Registraion", text: "Enter your password")
    }
    
    func clearKeyChaing() {
        let secItemClasses = [kSecClassGenericPassword, kSecClassInternetPassword, kSecClassCertificate, kSecClassKey, kSecClassIdentity]
        for itemClass in secItemClasses {
            let spec: NSDictionary = [kSecClass: itemClass]
            SecItemDelete(spec)
        }
    }
    
    func showAlert(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        let okAction = UIAlertAction(title: "OK", style: .destructive) { (_) in
            if let textField = alert.textFields?.first {
                let fieldPassword = String(textField.text!)
                if self.password == fieldPassword {
                    self.nextStep
                    {
                        self.spinner.stopAnimating()
                        self.spinner.isHidden = true
                    }
                } else if self.password == "" {
                    self.showRegAlert(title: "Registraion", text: "Enter your password")
                }
            }
        }
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRegAlert(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        let okAction = UIAlertAction(title: "OK", style: .destructive) { [self] (_) in
            if let textField = alert.textFields?.first {
                let fieldPassword = String(textField.text!)
                self.password = fieldPassword
                do {
                    try keychain.set(fieldPassword, for : accessTokenKey)
                } catch let error {
                    debugPrint(error)
                }
            }
        }
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func nextStep(complition: @escaping () -> ()) {
        if !UserDefaults.isFirstLaunch() {
            guard let startGame = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            self.navigationController?.pushViewController(startGame, animated: true)
            UserDefaults.setFirstLaunch(true)
        } else {
            showSpinnerView()
            guard let startGame = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                self.navigationController?.pushViewController(startGame, animated: true)
            complition()
        }
    }
    
    func showSpinnerView() {
        spinner.isHidden = false
        spinner.color = .white
        spinner.startAnimating()
        
    }
}

