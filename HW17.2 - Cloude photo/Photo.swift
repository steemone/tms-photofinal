import UIKit
import Foundation

class Photo: Codable {
    var comment: String?
    var favorite: Bool?
    let name: String?
    
    
    init(comment: String?, favorite: Bool?, name: String?) {
        self.comment = comment
        self.favorite = favorite
        self.name = name
        
    }
    private enum CodingKeys: String, CodingKey {
        case comment
        case favorite
        case name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        favorite = try container.decodeIfPresent(Bool.self, forKey: .favorite)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.favorite, forKey: .favorite)
        try container.encode(self.name, forKey: .name)
        
    }
    
}


extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}


