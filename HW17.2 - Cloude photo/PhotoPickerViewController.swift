import UIKit

enum SwipePictures {
    case back
    case forward
}

class PhotoPickerViewController: UIViewController {
    
    @IBOutlet var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet var smallSizeImage: UIImageView!
    @IBOutlet var bigSizeImage: UIImageView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var enterCommentLabel: UITextField!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet weak var favoriteButtonOutlet: UIButton!
    
    var smallPictureView = UIImageView()
    var bigPictureView = UIImageView()
    var smallSizeImageSecond = UIImageView()
    var currentIndex = 0
    var photoArray: [Photo] = []
    var fullScreenSize = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        guard let element = UserDefaults.standard.value([Photo].self, forKey: "photo") else {return}
        self.photoArray = element
        guard let image = self.loadImage(fileName: (photoArray.first?.name)!) else { return }
        self.smallSizeImage.image = image
        setupButton()
    }
    
    @IBAction func favoriteButton(_ sender: UIButton) {
        if photoArray[currentIndex].favorite == false {
            photoArray[currentIndex].favorite = true
            self.favoriteButtonOutlet.setImage(UIImage(named: "likeSecond"), for: .normal)
        } else {
            photoArray[currentIndex].favorite = false
            self.favoriteButtonOutlet.setImage(UIImage(named: "likeFirst"), for: .normal)
        }
        let element = photoArray
        UserDefaults.standard.set(encodable: element, forKey: "photo")
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        let element = photoArray
        UserDefaults.standard.set(encodable: element, forKey: "photo")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCommentButton(_ sender: UIButton) {
        let comment = String(enterCommentLabel.text!)
        photoArray[currentIndex].comment = comment
        enterCommentLabel.text = ""
        let element = photoArray
        UserDefaults.standard.set(encodable: element, forKey: "photo")
    }
    @IBAction func leftSwipe(_ sender: UIButton) {
        swipePicture(.back)
    }
    
    @IBAction func rightSwipe(_ sender: UIButton) {
        swipePicture(.forward)
    }
    
    func setupButton() {
        self.favoriteButtonOutlet.setImage(UIImage(named: "likeFirst"), for: .normal)
    }
    
    func shiftIndex () {
        if currentIndex < photoArray.count - photoArray.count {
            currentIndex = photoArray.count - 1
        } else if currentIndex > photoArray.count - 1 {
            currentIndex = photoArray.count - photoArray.count
        }
    }
   
    func swipePicture(_ swipe: SwipePictures) {
        switch swipe {
        case .forward:
            currentIndex += 1
            shiftIndex()
            smallPictureView.frame = CGRect(x: self.view.frame.maxX,
                                            y: self.smallSizeImage.frame.origin.y,
                                            width: self.smallSizeImage.frame.size.width,
                                            height: self.smallSizeImage.frame.size.height)
            guard let image = self.loadImage(fileName: (photoArray[currentIndex].name!)) else { return }
            smallPictureView.image = image
            smallPictureView.contentMode = .scaleAspectFit
            self.view.addSubview(smallPictureView)
            commentLabel.text = photoArray[currentIndex].comment
            if photoArray[currentIndex].favorite == false {
                self.favoriteButtonOutlet.setImage(UIImage(named: "likeFirst"), for: .normal)
            } else if photoArray[currentIndex].favorite == true {
                self.favoriteButtonOutlet.setImage(UIImage(named: "likeSecond"), for: .normal)
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
                self.smallPictureView.frame.origin.x = self.smallSizeImage.frame.origin.x
            } completion: { (_) in
                self.smallSizeImage.image = self.smallPictureView.image
                self.smallPictureView.removeFromSuperview()
            }
        case .back:
            currentIndex -= 1
            shiftIndex()
            smallPictureView.frame = CGRect(x: self.smallSizeImage.frame.origin.x,
                                            y: self.smallSizeImage.frame.origin.y,
                                            width: self.smallSizeImage.frame.size.width,
                                            height: self.smallSizeImage.frame.size.height)
            guard let image = self.loadImage(fileName: (photoArray[currentIndex].name)!) else { return }
            smallPictureView.image = smallSizeImage.image
            smallPictureView.contentMode = .scaleAspectFit
            self.view.addSubview(smallPictureView)
            smallSizeImage.image = image
            commentLabel.text = photoArray[currentIndex].comment
            if photoArray[currentIndex].favorite == false {
                self.favoriteButtonOutlet.setImage(UIImage(named: "likeFirst"), for: .normal)
            } else if photoArray[currentIndex].favorite == true {
                self.favoriteButtonOutlet.setImage(UIImage(named: "likeSecond"), for: .normal)
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
                self.smallPictureView.frame.origin.x -= self.smallPictureView.frame.size.width
            }completion: { (_) in
                self.smallPictureView.removeFromSuperview()
            }
            
        }
    }
    func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image da")
            } catch let error {
                print("couldn't remove file at path", error)
            }
        }
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomViewConstraint.constant = 0
        } else {
            bottomViewConstraint.constant = keyboardScreenEndFrame.height + 10
        }
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension PhotoPickerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
