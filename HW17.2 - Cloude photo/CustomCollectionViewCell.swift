//
//  CustomCollectionViewCell.swift
//  HW17.2 - Cloude photo
//
//  Created by Валерий Долгий on 18.02.21.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    var photoArray: [Photo] = []
    var gestureCallback: () -> () = {}
    
    @IBOutlet weak var myImage: UIImageView!
    
    func configure(object: Photo) {
        guard let image = self.loadImage(fileName: (object.name!)) else { return }
        myImage.contentMode = .scaleAspectFill
        myImage.image = image
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(loadScreen))
        self.addGestureRecognizer(recognizer)
    }
    
    func loadImage(fileName: String) -> UIImage? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageUrl = documentsDirectory.appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
    @objc func loadScreen() {
        gestureCallback()

    }
    
    
}
