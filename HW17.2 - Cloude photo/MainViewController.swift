import UIKit
import Foundation

class MainViewController: UIViewController {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var photoArray: [Photo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let element = UserDefaults.standard.value([Photo].self, forKey: "photo") else {return}
        self.photoArray = element
        collectionView.reloadData()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionView.reloadData()
    }
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addPhotoButton(_ sender: UIButton) {
        showAlert(title: "", text: "Загрузить фото из:")
    }
    func nextStep() {
        guard let startGame = self.storyboard?.instantiateViewController(withIdentifier: "PhotoPickerViewController") as? PhotoPickerViewController else { return }
        self.navigationController?.pushViewController(startGame, animated: true)
    }
    func showAlert(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (_) in
            self.performImageCamera()
        }
        alert.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Галерея", style: .default) { (_) in
            self.performImagePicker()
        }
        alert.addAction(galleryAction)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    private func performImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    private func performImageCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        let element = UserDefaults.standard.value([Photo].self, forKey: "photo")
        cell.photoArray = element!
        if cell.photoArray.first == nil {
            let alert = UIAlertController(title: title, message: "Галерея пуста", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Назад", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        cell.configure(object: self.photoArray[indexPath.item])
        cell.gestureCallback = {
            guard let startGame = self.storyboard?.instantiateViewController(withIdentifier: "PhotoPickerViewController") as? PhotoPickerViewController else { return }
            let element = UserDefaults.standard.value([Photo].self, forKey: "photo")
            startGame.photoArray = element!
            if startGame.photoArray.first == nil {
                let alert = UIAlertController(title: self.title, message: "Галерея пуста", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Назад", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.navigationController?.pushViewController(startGame, animated: true)
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side: CGFloat = (view.frame.size.width - 10)/2
        return CGSize(width: side, height: side)
    }
    
}

